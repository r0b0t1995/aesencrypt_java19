package BackEnd;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Base64;

public class Aes {

    private SecretKey secretKey = null;

    private Cipher cipher = null;

    public String EncryptAes(String plainText, String secretKey){
        try {

            SecretKey key = generateKey( secretKey );

            cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");

            cipher.init(Cipher.ENCRYPT_MODE, key);

            return Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes("UTF-8")));

        }catch (Exception e){
            System.out.println("Error while encryption: " + e);
        }
        return null;
    }

    public String DecryptAes(String aesHash, String secretKey){

        try {

            SecretKey key = generateKey(secretKey);

            cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");

            cipher.init(Cipher.DECRYPT_MODE, key);

            return new String(cipher.doFinal(Base64.getDecoder().decode(aesHash)));

        }catch (Exception e){
            System.err.println("Error while decrypting: " + e);
        }
        return null;
    }

    private SecretKey generateKey( String secret ){

        try {

            byte[] key = secret.getBytes();

            secretKey = new SecretKeySpec(key, "AES");

        }catch (UnsupportedCharsetException e){
            e.printStackTrace();
        }

        return secretKey;
    }

}
