package Form;

import BackEnd.Aes;

import javax.swing.*;
import java.awt.event.*;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class AesForm {
    private JPanel MainPanel;
    private JButton btnEncrypt;
    private JButton btnDecrypt;
    private JTextField txtText;
    private JPasswordField txtPassword;
    private JLabel lblText;
    private JLabel lblPassword;
    private JTextField txtResult;
    private JLabel lblResult;
    private JButton btnViewPassword;

    private StringBuilder stringBuilder;
    private Aes aes;

    private JFrame jFrame;
    public AesForm(){
        jFrame = new JFrame("Encrypt and Decrypt Form");
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setContentPane(MainPanel);
        jFrame.setResizable(true);
        jFrame.setVisible(true);
        jFrame.pack();
        jFrame.setSize(450, 400);
        jFrame.setLocationRelativeTo(null);

        aes = new Aes();

        buttonEvent();
        textFieldsEvent();
    }

    private void buttonEvent(){

        btnEncrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(validateTextFields()){

                    String text = txtText.getText().strip();

                    String password = textPassword();

                    String hash = aes.EncryptAes(text, password);

                    txtResult.setText( hash );

                }else{

                    String passwordLength = String.valueOf(textPassword().length());

                    String  warning = String.format("You need to fill the text field and key length equals 16 \n" +
                            "actually your password length is: %s", passwordLength );

                    JOptionPane.showMessageDialog(null, warning, ":(", JOptionPane.ERROR_MESSAGE);

                }

            }
        });

        btnDecrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(validateTextFields()){

                    String text = txtText.getText().strip();

                    String password = textPassword();

                    String hash = aes.DecryptAes(text, password);

                    txtResult.setText(hash);

                }else{
                    String passwordLength = String.valueOf(textPassword().length());

                    String  warning = String.format("You need to fill the text field and key length equals 16 \n" +
                            "actually your password length is: %s", passwordLength );

                    JOptionPane.showMessageDialog(null, warning, ":(", JOptionPane.ERROR_MESSAGE);
                }

            }
        });


        btnViewPassword.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);

                    txtPassword.setEchoChar((char)0);

            }
        });

    }


    private boolean validateTextFields(){

        return (!txtText.getText().isBlank() && validatePassword());

    }

    public boolean validatePassword(){

        String password = textPassword();

        return password.length() == 16;

    }

    private void textFieldsEvent(){

        txtPassword.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if(textPassword().length() >= 16){
                    e.consume();
                }
            }
        });

    }

    private String textPassword(){

        stringBuilder = new StringBuilder();

        for(char item:txtPassword.getPassword()){

            stringBuilder.append(item);

        }

        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        new AesForm();
    }
}
